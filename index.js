
const url = 'https://rickandmortyapi.com/api/character';
const container = document.getElementById('container');
const search = document.getElementById('search');
let characters = [];
let sortByDateValue = 'ASC';
let sortByEpisodesValue = 'ASC';
let searchWord= '';

let pageSize = 10;

container.addEventListener('scroll', function() {
    if (Math.ceil(container.scrollTop) + container.clientHeight >= container.scrollHeight) {
        handleScroll();
    }
});

search.oninput = function() {
    searchWord = search.value;
    render(characters);
};

function handleScroll() {
    pageSize = pageSize + 10;
    render(characters);
}

function skipScroll() {
    container.scrollTo(0, 0);
    pageSize = 10;
    render(characters);
}

function render(characters) {
    container.innerHTML = ''
    characters
        .filter(x => x.name.includes(searchWord))
        .slice(0, pageSize)
        .forEach(character => {

        let created = character.created;
        let species = character.species;
        let img = character.image;
        let episodes = character.episode;
        let name = character.name;
        let location = character.location;

        const card = document.createElement("div");
        card.className = "card";

        const mainInfoBlock = document.createElement("div");
        mainInfoBlock.className = "main-info";

        const nameElement = document.createElement("div");
        nameElement.innerText = name;

        const imageEl = document.createElement("img");
        imageEl.setAttribute('src', img);

        const speciesElement = document.createElement("div");
        speciesElement.innerText = species;

        const createdElement = document.createElement("div");
        createdElement.innerText = created;

        const locationElement = document.createElement("div");
        locationElement.innerText = location.name;

        const episodesElement = document.createElement("ul");

        episodes.forEach(episode => {
            const episodeElement = document.createElement("li");
            episodeElement.innerText = episode;
            episodesElement.appendChild(episodeElement);
        });

        const deleteBtn = document.createElement("button");
        deleteBtn.innerText = "Delete";
        deleteBtn.onclick = () => {
            const id = character.id;
            characters = characters.filter(x => x.id !== id);
            render(characters);
        }

        mainInfoBlock.appendChild(nameElement);
        mainInfoBlock.appendChild(speciesElement);
        mainInfoBlock.appendChild(locationElement);
        mainInfoBlock.appendChild(createdElement);
        mainInfoBlock.appendChild(episodesElement);
        mainInfoBlock.appendChild(deleteBtn);

        card.appendChild(imageEl);
        card.appendChild(mainInfoBlock);

        container.appendChild(card);

    });
}

fetch(url)
    .then(res => res.json())
    .then(data => {
        characters = data.results;
        sortByDate();
        render(characters)
    })
    .catch((error) => {
        console.log(JSON.stringify(error));
    });

