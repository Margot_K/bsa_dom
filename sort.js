function sortByDate() {
    if (sortByDateValue === "ASC") {
        sortByDateValue = "DESC";
        characters = characters.sort((a, b) => {
            return new Date(a.created) - new Date(b.created);
        });
    }
    else {
        sortByDateValue = "ASC";
        characters = characters.sort((a, b) => {
            return new Date(b.created) - new Date(a.created);
        });
    }

    render(characters)
}

function sortByEpisodes() {
    if (sortByEpisodesValue === "ASC") {
        sortByEpisodesValue = "DESC";
        characters = characters.sort((ob1, ob2) => sortAsc(ob1, ob2));
    }
    else {
        sortByEpisodesValue = "ASC";
        characters = characters.sort((ob1, ob2) => sortDesc(ob1, ob2));
    }

    render(characters)
}

function sortAsc(ob1,ob2) {
    if (ob1.episode.length > ob2.episode.length) {
        return 1;
    } else if (ob1.episode.length < ob2.episode.length) { 
        return -1;
    }

    
    if (new Date(ob1.created) < new Date(ob2.created)) { 
        return -1;
    } else if (new Date(ob1.created) > new Date(ob2.created)) {
        return 1
    } else { 
        return 0;
    }
}

function sortDesc(ob1,ob2) {
    if (ob1.episode.length < ob2.episode.length) {
        return 1;
    } else if (ob1.episode.length > ob2.episode.length) { 
        return -1;
    }

    
    if (new Date(ob1.created) > new Date(ob2.created)) { 
        return -1;
    } else if (new Date(ob1.created) < new Date(ob2.created)) {
        return 1
    } else {
        return 0;
    }
}
